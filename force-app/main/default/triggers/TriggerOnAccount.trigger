trigger TriggerOnAccount on Account (after insert, after update) 
{
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            try
            {
                triggerOnAccountHandler.createContacts(Trigger.New);
            }        
            catch(Exception e) 
            {
                System.debug(e.getMessage());
            }
        }
        if(Trigger.isUpdate)
        {
            try
            {
                triggerOnAccountHandler.createContacts(Trigger.New);
            }
            catch(Exception e) 
            {
                System.debug(e.getMessage());
            }
        }       
    }   
}