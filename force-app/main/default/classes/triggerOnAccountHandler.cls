public class triggerOnAccountHandler {
    public static void createContacts(List<Account> accounts)
    {
        List<Contact> contacts = new List<Contact>();
        for(Account a: accounts)
        {
            Contact newContact = new Contact(
                FirstName = a.Name,
                AccountId = a.Id);
            contacts.add(newContact);
        }
        if(contacts.size() > 0) 
        {
            insert contacts;
        }
    }
}