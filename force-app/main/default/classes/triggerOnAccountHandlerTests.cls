@isTest 
public class triggerOnAccountHandlerTests {
    @testSetup static void methodName() {
        Account acc = new Account();
        acc.Name = 'Noman';
        acc.AccountNumber = '123';
        insert acc;
    }
    @isTest static void testMethod1() {
        Account acct = [SELECT Id FROM Account WHERE Name='Noman' LIMIT 1];
        acct.Name = 'Usman';
        update acct;
        System.assertEquals('Usman', acct.Name);
    }
}